const CompanyList = (props) => {
    const {companies} = props;
    return (
        <table className="table table-striped table-sm mt-10">
            <thead>
            <tr>
                <th>Name</th>
                <th>Type</th>
                <th>Nature of business</th>
                <th>Date of incorporation</th>
                <th>Email</th>
                <th>Phone number</th>
            </tr>
            </thead>
            <tbody>
            {companies ? companies.map(company => (
                <tr key={company.id}>
                    <td>{company ? company.companyName : 'Not Supplied'}</td>
                    <td>{company ? company.companyType : 'Not Supplied'}</td>
                    <td>{company ? company.natureofBusiness : 'Not Supplied'}</td>
                    <td>{company ? company.incorporatedDate : 'Not Supplied'}</td>
                    <td>{company ? company.emailAddress : 'Not Supplied'}</td>
                    <td>{company ? company.phoneNumber : 'Not Supplied'}</td>
                </tr>
            )) : null}
            </tbody>
        </table>
    )
}

export default CompanyList
