import React from 'react';
import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";

const CompanyCreate = (props) => {
    const { register, handleSubmit, errors } = useForm();
    const history = useHistory();

    const onSubmit = company => {
        props.onCreateCompany(company);
        history.push('/')
    }

    return (
        <div className="row">
            <div className="col-md-12">
                <h4 className="mb-3">Create Company</h4>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="mb-3">
                        <label htmlFor="name">Name</label>
                        <input className="form-control" name="companyName" ref={register({ required: true })}/>
                        {errors.companyName && <span className="text-danger">This field is required</span>}
                    </div>
                    <div className="mb-3">
                        <label htmlFor="companyType">Type</label>
                        <input className="form-control" name="companyType" ref={register({ required: true })}/>
                        {errors.companyType && <span className="text-danger">This field is required</span>}
                    </div>
                    <div className="mb-3">
                        <label htmlFor="natureofBusiness">Nature of business</label>
                        <input className="form-control" name="natureofBusiness" ref={register({ required: true })}/>
                        {errors.natureofBusiness && <span className="text-danger">This field is required</span>}
                    </div>
                    <div className="mb-3">
                        <label htmlFor="incorporatedDate">Date of incorporation</label>
                        <input className="form-control" name="incorporatedDate" ref={register({ required: true })}/>
                        {errors.incorporatedDate && <span className="text-danger">This field is required</span>}
                    </div>
                    <div className="mb-3">
                        <label htmlFor="emailAddress">Email</label>
                        <input className="form-control" name="emailAddress" ref={register({ required: true })}/>
                        {errors.emailAddress && <span className="text-danger">This field is required</span>}
                    </div>
                    <div className="mb-3">
                        <label htmlFor="phoneNumber">Phone number</label>
                        <input className="form-control" name="phoneNumber" ref={register({ required: true })}/>
                        {errors.phoneNumber && <span className="text-danger">This field is required</span>}
                    </div>
                    <hr className="mb-4"></hr>
                    <button data-testid="submitButton" className="btn btn-primary btn-md btn-block" type="submit">Submit</button>
                </form>
            </div>
        </div>
    )
}

export default CompanyCreate
