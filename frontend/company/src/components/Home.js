import React, {useState, useEffect} from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import CompanyCreate from './CompanyCreate'
import CompanyList from "./CompanyList";

const Home = (props) => {
    const [companies, setCompanies] = useState([]);

    const onCreateCompany = (company) => {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(company)
        };
        fetch('http://localhost:8080/company', requestOptions)
            .then(async response => {
                const createdCompany = await response.json();
                if (!response.ok) {
                    return Promise.reject(response.status);
                }
                setCompanies([...companies, createdCompany])
            })
            .catch(error => {
                console.error('There was an error!', error);
            });
    }

    useEffect(() => {
        fetch('http://localhost:8080/company')
            .then(
                function(response) {
                    if (response.status !== 200) {
                        console.log('FAILED. Status Code: ' + response.status);
                        return;
                    }
                    response.json().then(function(data) {
                        setCompanies(data.items)
                    });
                }
            )
            .catch(function(err) {
                console.log('FAILED:-S', err);
            });
    }, [])

    return (
        <div className="container">
            <div className="py-5 text-center">
                <h2>Companies</h2>
            </div>
            <Router>
                <div>
                    <Switch>
                        <Route exact path="/">
                            <Link data-testid="createCompanyButton" className="btn btn-primary btn-md pull-left mb-4" to="/create">Create Company</Link>
                            <CompanyList companies={companies}></CompanyList>
                        </Route>
                        <Route path="/create">
                            <CompanyCreate onCreateCompany={onCreateCompany} history={props.history}></CompanyCreate>
                        </Route>
                    </Switch>
                </div>
            </Router>
            <footer className="my-5 pt-5 text-muted text-center text-small">
                <p className="mb-1">&copy; 2020 Onestop Tech Limited</p>
            </footer>
        </div>
    )
}

export default Home
