import { render, screen, fireEvent } from '@testing-library/react';
import App from './App';

test('renders Companies header', () => {
  render(<App />);
  const header = screen.getByText(/Companies/i);
  expect(header).toBeInTheDocument();
});

test('renders "Create Company" button', () => {
  render(<App />);
  const createCompanyButton = screen.getByTestId("createCompanyButton");
  expect(createCompanyButton).toBeInTheDocument();
});

test('renders "Companies" table headers', () => {
  render(<App />);

  expect(screen.getByText(/Name/i)).toBeInTheDocument();
  expect(screen.getByText(/Type/i)).toBeInTheDocument();
  expect(screen.getByText(/Nature of business/i)).toBeInTheDocument();
  expect(screen.getByText(/Date of incorporation/i)).toBeInTheDocument();
  expect(screen.getByText(/Email/i)).toBeInTheDocument();
  expect(screen.getByText(/Phone number/i)).toBeInTheDocument();
});

test('clicks "Create Company" button and renders "Submit" button', () => {
  render(<App />);
  const createCompanyButton = screen.getByTestId("createCompanyButton");
  fireEvent.click(createCompanyButton)
  const submitButton = screen.getByTestId("submitButton");
  expect(submitButton).toBeInTheDocument();
});
