package com.sedex.connect

import com.fasterxml.jackson.databind.ObjectMapper
import com.sedex.connect.entity.Company
import com.sedex.connect.entity.CompanyAddress
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.time.LocalDateTime
import java.util.*

@AutoConfigureMockMvc
@SpringBootTest
class ApplicationTests {

	@Autowired
	lateinit var mockMvc: MockMvc

	@Test
	fun testCompaniesPostAndGetEndpoints() {
		postCompany()
		getCompaniesAndExpectations()
	}

	fun postCompany() {
		val payload = """
			{
				"companyName": "Marks & Spencers",
				"companyType": "Department store",
				"natureofBusiness": "Food and Clothing",
				"incorporatedDate": "2007-02-27",
				"emailAddress": "hello@mAnds.com",
				"phoneNumber": "0208777222",
				"address": {
					"addressLine1": "2-28",
					"addressLine2": "St Nicholas Street",
					"city": "Aberdeen",
					"state": "Aberdeen",
					"postalCode": "AB10 1BU",
					"countryCode": "UK"
				}
			}
		""".trimIndent()
		mockMvc.perform(
			MockMvcRequestBuilders.
					post("/company")
				.contentType(MediaType.APPLICATION_JSON)
				.content(payload)
				.accept(MediaType.APPLICATION_JSON)
		).andExpect(MockMvcResultMatchers.status().isOk)
	}

	fun getCompaniesAndExpectations() {
		mockMvc.perform(
			MockMvcRequestBuilders
				.get("/company")
				.accept(MediaType.APPLICATION_JSON))
			.andDo(MockMvcResultHandlers.print())
			.andExpect(MockMvcResultMatchers.status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.items").exists())
			// .andExpect(MockMvcResultMatchers.jsonPath("$.items[0].id").value(""))
			.andExpect(MockMvcResultMatchers.jsonPath("$.items[0].companyName").value("Marks & Spencers"))
			.andExpect(MockMvcResultMatchers.jsonPath("$.items[0].companyType").value("Department store"))
			.andExpect(MockMvcResultMatchers.jsonPath("$.items[0].natureofBusiness").value("Food and Clothing"))
			// .andExpect(MockMvcResultMatchers.jsonPath("$.items[0].incorporatedDate").value("2007-02-27"))
			.andExpect(MockMvcResultMatchers.jsonPath("$.items[0].emailAddress").value("hello@mAnds.com"))
			.andExpect(MockMvcResultMatchers.jsonPath("$.items[0].phoneNumber").value("0208777222"))
			// .andExpect(MockMvcResultMatchers.jsonPath("$.items[0].createdTime").value(""))
			// .andExpect(MockMvcResultMatchers.jsonPath("$.items[0].updatedTime").value(""))
			/*.andExpect(MockMvcResultMatchers.jsonPath("$.items[0].address").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.items[0].address.addressLine1").value("2-28"))
			.andExpect(MockMvcResultMatchers.jsonPath("$.items[0].address.addressLine2").value("St Nicholas Street"))
			.andExpect(MockMvcResultMatchers.jsonPath("$.items[0].address.city").value("Aberdeen"))
			.andExpect(MockMvcResultMatchers.jsonPath("$.items[0].address.state").value("Aberdeen"))
			.andExpect(MockMvcResultMatchers.jsonPath("$.items[0].address.postalCode").value("AB10 1BU"))
			.andExpect(MockMvcResultMatchers.jsonPath("$.items[0].address.countryCode").value("UK"))*/
	}

}
