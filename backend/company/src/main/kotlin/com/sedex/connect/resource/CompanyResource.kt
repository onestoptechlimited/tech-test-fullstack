package com.sedex.connect.resource

import com.sedex.connect.entity.Companies
import com.sedex.connect.entity.Company
import com.sedex.connect.services.CompanyService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class CompanyResource(val companyService: CompanyService) {

    @GetMapping("/company")
    fun getCompanies(): ResponseEntity<Companies> {
        return ResponseEntity<Companies>(Companies(companyService.getCompanies()), HttpStatus.OK)
    }

    @PostMapping("/company")
    fun createCompany(@RequestBody company: Company): ResponseEntity<Company> {
        val company = companyService.createCompany(company)
        return ResponseEntity<Company>(company, HttpStatus.OK)
    }

}
