package com.sedex.connect.services

import com.sedex.connect.entity.Company
import com.sedex.connect.repository.CompanyRepository
import org.springframework.stereotype.Service
import java.util.*

@Service
class CompanyService(val companyRepository: CompanyRepository) {

    fun getCompanies(): List<Company> {
        return companyRepository.findAll()
    }

    fun createCompany(company: Company): Company {
        company.id = UUID.randomUUID()
        companyRepository.save(company)
        return company
    }

}
