package com.sedex.connect.entity

import lombok.Data
import java.util.*
import javax.persistence.*

@Entity
@Data
class CompanyAddress {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "company_id")
    private val id: UUID? = null

    @OneToOne
    @MapsId
    @JoinColumn(name = "company_id")
    private val company: Company? = null

    @Column(name = "address_line1")
    private val addressLine1: String? = null

    @Column(name = "address_line2")
    private val addressLine2: String? = null

    @Column(name = "city")
    private val city: String? = null

    @Column(name = "state")
    private val state: String? = null

    @Column(name = "postal_code")
    private val postalCode: String? = null

    @Column(name = "country_code")
    private val countryCode: String? = null

}
