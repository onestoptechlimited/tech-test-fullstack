package com.sedex.connect.entity

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import lombok.Data
import java.util.*
import javax.persistence.*

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
class Company {

    @Id
    @Column(name = "id")
    lateinit var id: UUID

    /*@OneToOne(mappedBy = "company")
    @PrimaryKeyJoinColumn
    lateinit var address: CompanyAddress*/

    @Column(name = "companyType")
    lateinit var companyType: String

    @Column(name = "companyName")
    lateinit var companyName: String

    @Column(name = "natureofBusiness")
    lateinit var natureofBusiness: String

    @Column(name = "incorporatedDate")
    lateinit var incorporatedDate: String

    @Column(name = "emailAddress")
    lateinit var emailAddress: String

    @Column(name = "phoneNumber")
    lateinit var phoneNumber: String

}
