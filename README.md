# Sedex Connect - Fullstack Tech Test

This is an example technical test for a full stack developer. 💻

It aims to give the interviewee the opportunity to show their knowledge by implementing the `POST /company` 
method of the [open api spec](./companies-openapi3.yaml) in either the frontend OR the backend.  

### Frontend
If you choose the to implement frontend please do so in React, feel free to use whatever libs to assist your 
implementation but please include the dependencies in the build tool:

- Create a page that will allow a user to submit a company via a form and display the response.
- You can use the following endpoint to implement against: https://sedex-apim-gateway.cloud.gravitee.io/interview/v0/...


### Backend
If you choose to implement the backend please do Kotlin, feel free to use whatever libs to assist your implementation 
but please include the dependencies in the build tool.

- Expose the endpoint as a Restful HTTP service consuming and returning a JSON response as per the api spec.

### Solution structure

To start please fork this repository, make publicly accessible so that we can review at the end. 

We are looking for clean and tested code. Please provide a courteous notes appended to this README on tools and libs 
you might have used and with instructions on how to test and run the application locally.  

The project can be organised you see fit, in this repo are some example front and backend projects to point you in the
 right direction, but feel free to throw away and implement your version as you see fit, however we do ask that all the 
 code required to run the solution is submitted to a to your public fork of this repo.

Please time box the exercise to  a hour and a half maximum. Git commits would be appreciated to show the solutions 
evolution. 

If you cant implement the full POST company specification in an hour and a half, dont worry, just put down a few steps 
that you would go on to implement to complete the spec and the time you estimate that it would take to complete.

Thanks! 😃

## Assignments complete
### Assignment 1
* Did a commit saying `on marks, get set, go` when first started
* Converted to Spring boot BE and then took a TDD approach and committed after tests complete: `ASSIGNMENT 1: kotlin convert to Spring boot and TDD tests`
* Worked on implementation but ran out of time so commented out test assertions and had not got these quite right, but API served 200 status. Then committed: `ASSIGNMENT 1: kotlin implementation`
* Stopped for a bit before assignment 2, see commit `Break for a bit - kids bedtime routine`

### Assignment 2
* Did a commit saying `ASSIGNMENT 2: on marks, get set, go` when restarted after kids bedtime routine complete
* Solution:
  * Started with implementation this time (learned my lesson from Assignment 1 - too ambitious with TDD approach previously)
  * Worked through front-end implementation but found plugging into API was not working for a few reasons: 
     * CORS issues
     * Deserialisation of entities
     * UUID generation
  * Fixed all of the above in back-end side (thought worth it as e2e solution would be more impressive)
  * Did as much test coverage as had time to do in FE

### If I had more time...
* Would get the address fields functional - I commented this functionality out in back-end when realised not working as would have taken too long to fix
* Once the above fixed would:
  * Add back in commented-out code for BE integration tests
  * Extended FE form/table to include address fields
* Add more FE test coverage
* Add better exception handing in the BE 
* Add validation in the FE

## Run project and test manually
* Import project into `Intellij`
* Run back-end
   * in backend/company/src
   * open com.sedex.connect.Application
   * right-click > `Run Application`
* In Intellij `Terminal` tab:
  * Write command: ` cd ../../frontend/company/`
  * Hit return
  * Install node modules: `yarn install`
  * Run application: `yarn start`
* Test manually    
  * In browser, go to: `http://localhost:3000/`
  * Click `Create company` button
  * Hit `Submit` button
  * Notice validation messages
  * Fill in form
  * Hit `Submit` button
  * Should be redirected to table of results with entered company listed

## Run Automated Tests
* For back-end:
  * Import project into `Intellij`
  * Open `com.sedex.connect.ApplicationTests.kt`
  * right-click  > `Run ApplicationTests`
* For front-end:
 * Import project into `Intellij`
 * Open `App.test.js`
 * right-click  > `App.test.js`
